package example.gltest;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Anton on 9/23/2014.
 */
public class ItemsAdapter extends ArrayAdapter<Integer> {
    private final Context mContext;
    public ItemsAdapter(Context context, int resource, List<Integer> objects) {
        super(context, resource, objects);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }
}
