package example.gltest;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;

import java.util.HashMap;
import java.util.Map;

import example.gltest.fragments.EditFragment;
import example.gltest.fragments.PreviewFragment;


public class MainActivity extends Activity implements TabHost.OnTabChangeListener {

    private TabHost mTabHost;
    private final String EDIT_TAG = "edit";
    private final String PREVIEW_TAG = "preview";
    private Map<String, Fragment> fragmentMap = new HashMap<String, Fragment>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTabHost = (TabHost) findViewById(R.id.tabHost);
        mTabHost.setup();
        addTabToTabHost(EDIT_TAG, getString(R.string.edit));
        addTabToTabHost(PREVIEW_TAG, getString(R.string.preview));
        fragmentMap.put(EDIT_TAG, new EditFragment());
        fragmentMap.put(PREVIEW_TAG, new PreviewFragment());
        mTabHost.setOnTabChangedListener(this);
        changeTab(EDIT_TAG);
    }

    private void applyValues(){
        EditFragment editFragment = (EditFragment) fragmentMap.get(EDIT_TAG);
        PreviewFragment previewFragment = (PreviewFragment) fragmentMap.get(PREVIEW_TAG);
        if(editFragment != null && previewFragment != null){
            previewFragment.setValues(editFragment.getValues());
        }
    }

    private void addTabToTabHost(String tag, String indicator) {
        TabHost.TabSpec spec = mTabHost.newTabSpec(tag);
        spec.setContent(new TabHost.TabContentFactory() {
            @Override
            public View createTabContent(String tag) {
                return findViewById(android.R.id.tabcontent);
            }
        });
        spec.setIndicator(indicator);
        mTabHost.addTab(spec);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            final Fragment edit = fragmentMap.get(EDIT_TAG);
            if(edit instanceof EditFragment){
                EditFragment editFragment = (EditFragment) edit;
                editFragment.addItem();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabChanged(String tabId) {
        changeTab(tabId);
        applyValues();
    }

    private void changeTab(String tabId) {
        Fragment currentFragment = getFragmentManager().findFragmentById(android.R.id.tabcontent);
        Fragment newTab = fragmentMap.get(tabId);
        FragmentManager manager = getFragmentManager();
        if (currentFragment != newTab) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
            if (newTab != null) {
                if (currentFragment != null) {
                    ft.detach(currentFragment);
                }
                if (manager.findFragmentByTag(tabId) == null) {
                    ft.add(android.R.id.tabcontent, newTab, tabId);
                } else {
                    ft.attach(newTab);
                }
            }
            ft.commit();
        }
    }
}
