package example.gltest.controls;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Anton on 9/22/2014.
 */
public class MyPieChart extends View {

    private Paint mPaint;
    private RectF arc_bounds;
    private List<Integer> mDataValues;
    private int value_sum;
    private float mArcFilledPercent;
    private int[] mColorValues = {Color.CYAN, Color.BLUE, Color.GREEN, Color.RED, Color.WHITE, Color.MAGENTA};
    private Paint linePaint;
    public static final int MAX_PERCENTS = 100;

    public MyPieChart(Context context, AttributeSet attrs) {
        super(context, attrs);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(0.5f);

        linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeJoin(Paint.Join.ROUND);
        linePaint.setStrokeCap(Paint.Cap.ROUND);
        linePaint.setStrokeWidth(0.5f);
        linePaint.setColor(Color.BLACK);

        if(isInEditMode()){
            mDataValues = Arrays.asList(5, 14, 43, 2, 55, 98);
            calculateTotalValueSumm();
        }
    }

    private void calculateTotalValueSumm() {
        for (int datum : mDataValues)
            value_sum += datum;
    }

    public void setArcFilledPercent(float percent) {
        if(percent < 0){
            mArcFilledPercent = 0;
        } else if (percent > MAX_PERCENTS){
            mArcFilledPercent = MAX_PERCENTS;
        } else {
            mArcFilledPercent = percent;
        }
        invalidate();
    }

    public float getArcFilledPercent() {
        return mArcFilledPercent;
    }

    public void setValues(List<Integer> values){
        if(values != null){
            if(mDataValues == null){
                mDataValues = new ArrayList<Integer>();
            }
            mDataValues.clear();
            mDataValues.addAll(values);
            calculateTotalValueSumm();
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(mDataValues == null) return;
        calculateRectForArcs();
        float startAngle = 0;
        int arcIndex = 0;

        for (int datum : mDataValues) {
            if (datum == 0) continue;

            float endAngle = value_sum == 0 ? 0 : 360 * datum / (float) value_sum;
            float filledAngleEnd = endAngle * ((mArcFilledPercent) / MAX_PERCENTS);
            float newStartAngle = startAngle + endAngle;


            int color = mColorValues[arcIndex % mColorValues.length];
            mPaint.setColor(color);

            canvas.drawArc(arc_bounds, startAngle, filledAngleEnd, true, mPaint);
            canvas.drawArc(arc_bounds, startAngle, endAngle, true, linePaint);

            startAngle = newStartAngle;
            arcIndex++;
        }
    }

    private void calculateRectForArcs() {
        int rectSize = Math.min(getWidth(), getHeight());
        arc_bounds = new RectF(0, 0, rectSize, rectSize);
    }
}
