package example.gltest.fragments;



import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;

import java.util.List;

import example.gltest.controls.MyPieChart;
import example.gltest.R;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class PreviewFragment extends Fragment {


    private MyPieChart mChart;
    private List<Integer> mValues;

    public PreviewFragment() {
        // Required empty public constructor
    }


    public void setValues(List<Integer> values){
        if(values != null) {
            mValues = values;
        }
        if(mChart != null){
            mChart.setValues(values);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        animateChart(true);
    }

    private void animateChart(boolean delayed) {
        Animator animator = ObjectAnimator.ofFloat(mChart, "arcFilledPercent", 0, MyPieChart.MAX_PERCENTS);
        if(delayed) {
            animator.setStartDelay(500);
        }
        animator.setDuration(800);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview, container, false);
        mChart = (MyPieChart) view.findViewById(R.id.chart);
        mChart.setValues(mValues);
        Button animateButton = (Button) view.findViewById(R.id.animate_button);
        animateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateChart(false);
            }
        });
        return view;
    }


}
