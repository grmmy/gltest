package example.gltest.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import example.gltest.R;


public class EditFragment extends Fragment {

    private ViewGroup mViewGroup;
    private Map<String, Integer> mValues;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mValues = new LinkedHashMap<String, Integer>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit, container, false);
        mViewGroup = (ViewGroup) view.findViewById(R.id.layout);
        initViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews() {
        for (String key : mValues.keySet()) {
            addItemWithValue(key, mValues.get(key));
        }
    }

    public List<Integer> getValues() {
        if (mValues == null) return null;
        return new ArrayList<Integer>(mValues.values());
    }

    public void addItem() {
        // Instantiate a new "row" view.
        final String text = "#" + String.valueOf(mViewGroup.getChildCount() + 1);
        final Integer value = 0;
        mValues.put(text, value);
        addItemWithValue(text, value);
    }

    private void addItemWithValue(final String text, Integer value) {
        final ViewGroup newView = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.item_layout, mViewGroup, false);
        TextView textView = (TextView) newView.findViewById(R.id.text);
        textView.setText(text);
        newView.findViewById(R.id.delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewGroup.removeView(newView);
                mValues.remove(text);
            }
        });
        mViewGroup.addView(newView, 0);

        //SeekBar bar = (SeekBar) newView.findViewById(R.id.seek);
        SeekBar bar = null;
        for(int index =0; index < newView.getChildCount(); index++){
            if(newView.getChildAt(index) instanceof SeekBar){
                bar = (SeekBar) newView.getChildAt(index);
                break;
            }
        }
        bar.setProgress(value);
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                           @Override
                                           public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                               if (fromUser) {
                                                   mValues.put(text, progress);
                                               }
                                           }

                                           @Override
                                           public void onStartTrackingTouch(SeekBar seekBar) {

                                           }

                                           @Override
                                           public void onStopTrackingTouch(SeekBar seekBar) {

                                           }
                                       }
        );
    }
}
